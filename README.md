This repository holds automated tests to test the Bluefin API

Pre-reqs before starting:
1) ruby 2.3.x +

Note: on MacOS you can use rvm to manage the rubies
On Windows, after installing ruby, must install MYSYS2 to allow for the compilation of gems that are written in C.

Instructions to get started:

MacOS / Windows / Linux
1) Clone repo
2) bundle install
3) run: bundle exec cucumber feature/<feature_to_be_tested>