Given(/^I set the (.*) header to (.*)$/) do |header, value|
  @content_type = value
  @bluefin_api_test.headers.merge!(header => value)
end

When(/I run the transaction post using the (.*) body$/) do |json|
  @bluefin_api_test.execute_ksn_decrypt(post_body: json)
end

Then("the response returned should be as expected") do
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.response.headers['content-type']).to include @content_type
end

Then(/^the response returned should have the correct (.*) and (.*)$/) do | card_number, card_info |
  expect(@bluefin_api_test.emv_tags).to include card_number && card_info
  expect(@bluefin_api_test.decryptor_used).to eql 'P2PE PCI - BlueFin'
end

Then("I should see the proper invalid expected response") do
  expect(@bluefin_api_test.response.code).to eql 500
  expect(@bluefin_api_test.response.body).to be_empty
end

When(/^I decrypt a predefined set of encrypted EMV Data$/) do
  @json_query = {EmvData: {Tags: '4F:a0000000041010|50:MASTERCARD|57:2223000000002020d191220100000000000000|82:3800|84:a0000000041010|95:0000088000|9A:171031|9B:e800|9C:00|1005:3030|100E:454e|5F2A:0840|5F34:00|9000:42|9001:43|9F02:000000000720|9F03:000000000000|9F06:a0000000041010|9F09:0002|9F10:0014a08003222000ae2200000000096000ff|9F11:01|9F1A:0840|9F1B:00000000|9F1E:3832333737303731|9F21:232111|9F26:c5e9b0f472efa83a|9F27:80|9F33:e0f8c8|9F34:1e0300|9F35:22|9F36:0042|9F37:8623e044|9F39:05|9F40:4000f09001|9F41:00000034|9F53:52|FF1F:323232333030323032303136313139313232303130303031423030303232383030303030303030343030303833303131343337343334303736383337343432313735313d363937363936303839363239353736393638383833323938424638434235464435464131324546374341373131423941393637413945393630414244423933394538313738384341434542343543333233344431464642313635383138434636354531303930463139353244413634373139314433423835433141334441363130374434353536324238393342373031364336313242434436|',
                           SerialNumber: '82377071'},
                 TrackData: {Value: '2223000000002020=191220100000000000000',
                             Format: 0,
                             IsContactless: false}}.to_json
  @bluefin_api_test.execute_ksn_decrypt(post_body: @json_query)
end

Then(/^I should see the proper decrypted response from the server$/) do
  hash_check = JSON.parse('{
                              "EmvData": {
                                  "Tags": "4F:a0000000041010|50:MASTERCARD|57:2223000010232020=19122010000000005060|82:3800|84:a0000000041010|95:0000088000|9A:171031|9B:e800|9C:00|1005:3030|100E:454e|5F2A:0840|5F34:00|9000:42|9001:43|9F02:000000000720|9F03:000000000000|9F06:a0000000041010|9F09:0002|9F10:0014a08003222000ae2200000000096000ff|9F11:01|9F1A:0840|9F1B:00000000|9F1E:3832333737303731|9F21:232111|9F26:c5e9b0f472efa83a|9F27:80|9F33:e0f8c8|9F34:1e0300|9F35:22|9F36:0042|9F37:8623e044|9F39:05|9F40:4000f09001|9F41:00000034|9F53:52|FF1F:323232333030323032303136313139313232303130303031423030303232383030303030303030343030303833303131343337343334303736383337343432313735313d363937363936303839363239353736393638383833323938424638434235464435464131324546374341373131423941393637413945393630414244423933394538313738384341434542343543333233344431464642313635383138434636354531303930463139353244413634373139314433423835433141334441363130374434353536324238393342373031364336313242434436|5A:2223000010232020",
                                  "SerialNumber": "82377071"
                              },
                              "TrackData": {
                                  "Value": "2223000010232020=19122010000000005060",
                                  "Format": null,
                                  "IsContactless": false
                              },
                              "CardData": null,
                              "DecryptorUsed": "P2PE PCI - BlueFin"
                          }')
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.emv_tags).to eql hash_check['EmvData']['Tags']
  expect(@bluefin_api_test.emv_serial_number).to eql hash_check['EmvData']['SerialNumber']
  expect(@bluefin_api_test.track_data_value).to eql hash_check['TrackData']['Value']
  expect(@bluefin_api_test.track_data_format).to be nil
  expect(@bluefin_api_test.track_data_is_contactless).to eql hash_check['TrackData']['IsContactless']
  expect(@bluefin_api_test.card_data).to be nil
  expect(@bluefin_api_test.decryptor_used).to eql hash_check['DecryptorUsed']
end

When("I attempt to decrypt invalid card data with KSN") do
  @json_query = {xyz: 'xyz'}.to_json
  @bluefin_api_test.execute_ksn_decrypt(post_body: @json_query)
end

Then("I should see the proper invalid response from the server") do
  expect(@bluefin_api_test.response.code).to eql 500
  expect(@bluefin_api_test.content_length.to_i).to eql 0
end

When("I attempt to decrypt empty json card data with KSN") do
  @json_query = {
      EmvData: {
          Tags: '',
          SerialNumber: ''
      },
      TrackData: {
          Value: '',
          Format: 'ClearText',
          IsContactless: true
      },
      CardData: {
          Number: '',
          Expiration: '',
          CVV: ''
      },
      ReferenceNumber: ''
  }.to_json
  @bluefin_api_test.execute_ksn_decrypt(post_body: @json_query)
end

Then("I should see the proper empty response returned from the server") do
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.emv_tags).to be_empty
  expect(@bluefin_api_test.emv_serial_number).to be_empty
  expect(@bluefin_api_test.response['TrackData']).to be_nil
  expect(@bluefin_api_test.response['CardData']).to be_nil
  expect(@bluefin_api_test.decryptor_used).to be_nil
end

When("I decrypt card data with KSN for swipe data") do
  json_query =   { EmvData: { Tags: "57:\u001c37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|", SerialNumber: '82377071'},
                   TrackData: { Value: "\u001c37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7", Format: 0, IsContactless: false}, DeviceId: 'CardPresent.TestClient', CardPresent: true,
                   Emv: { Tags: "57:\u001c37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|", SerialNumber: '82377071', KernelVersion: '0467'}
  }
  @bluefin_api_test.execute_ksn_decrypt(post_body: json_query.to_json)
end

Then("I should see the decrypted swipe data returned from the server") do
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.emv_tags).to eql '57:371449635398431=18121015432112345678|5A:371449635398431'
  expect(@bluefin_api_test.emv_serial_number).to eql '82377071'
  expect(@bluefin_api_test.track_data_value).to eql '371449635398431=18121015432112345678'
  expect(@bluefin_api_test.track_data_format).to be nil
  expect(@bluefin_api_test.track_data_is_contactless).to eql false
  expect(@bluefin_api_test.card_data).to be nil
  expect(@bluefin_api_test.decryptor_used).to eql 'P2PE PCI - BlueFin'
end

When("I decrypt card data with KSN for fallback swipe data one") do
  json_query =  {
      EmvData: {
          Tags:"57:\u001c541333043416118122011001B000228000000004000860114349895338966960554D38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50|",
          SerialNumber: "82377071"
      },
      TrackData: {
          Value: "\u001c541333043416118122011001B000228000000004000860114349895338966960554=38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50",
          Format: "0",
          IsContactless: false
      },
      ReferenceNumber:"123"
  }
  @bluefin_api_test.execute_ksn_decrypt(post_body: json_query.to_json)
end

Then("I should see the decrypted swipe data returned for fallback data swipe one") do
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.emv_tags).to eql '57:5413330089010434=18122010489806601|5A:5413330089010434'
  expect(@bluefin_api_test.emv_serial_number).to eql '82377071'
  expect(@bluefin_api_test.track_data_value).to eql '5413330089010434=18122010489806601'
  expect(@bluefin_api_test.track_data_format).to be nil
  expect(@bluefin_api_test.track_data_is_contactless).to eql false
  expect(@bluefin_api_test.card_data).to be nil
  expect(@bluefin_api_test.decryptor_used).to eql 'P2PE PCI - BlueFin'
end

When("I decrypt card data with KSN for fallback swipe data two") do
  json_query = {
      EmvData: {
          Tags:"57:37144984311511812101817CHASE PAYMENTECH 1B0002280000000040006E011436319932734780679D8742561244699210254732498F02A11169872D96ADF73C4802E4B396397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|",
          SerialNumber: "82377071"
      },
      TrackData: {
          Value: "\u001c37144984311511812101817CHASE PAYMENTECH 1B0002280000000040006E011436319932734780679=8742561244699210254732498F02A11169872D96ADF73C4802E4B396397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7",

          Format: "0",
          IsContactless: false
      },
      ReferenceNumber:"123"
  }
  @bluefin_api_test.execute_ksn_decrypt(post_body: json_query.to_json)
end

Then("I should see the decrypted swipe data returned for fallback data swipe two") do
  expect(@bluefin_api_test.response.code).to eql 200
  expect(@bluefin_api_test.emv_tags).to eql '57:371449635398431=18121015432112345678|5A:371449635398431'
  expect(@bluefin_api_test.emv_serial_number).to eql '82377071'
  expect(@bluefin_api_test.track_data_value).to eql '371449635398431=18121015432112345678'
  expect(@bluefin_api_test.track_data_format).to be nil
  expect(@bluefin_api_test.track_data_is_contactless).to eql false
  expect(@bluefin_api_test.card_data).to be nil
  expect(@bluefin_api_test.decryptor_used).to eql 'P2PE PCI - BlueFin'
end