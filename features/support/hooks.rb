Before('@bluefin_api_test') do
  @bluefin_api_test = BluefinAPI::CardDecryptClient.new
  @bluefin_api_test.query = { deviceType:'IPP320' }
  @bluefin_api_test.headers = {}
end

Before('@bankcard') do
  @bankcard = BluefinAPI::BankcardClient.new
  @bankcard.query = {}
  @bankcard.headers = {'Content-Type' => 'application/json'}
end