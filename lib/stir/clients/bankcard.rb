module BluefinAPI
  class BankcardClient < Stir::RestClient

    self.config_file = File.join(Stir.path, 'config', "bankcard_api.yml")

    post(:transaction) { '/Bankcard/Transactions' }

    def process_transaction(post_body:)
      transaction(body: post_body)
    end
  end
end